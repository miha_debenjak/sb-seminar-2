import cv2, sys, os, numpy as np


class Detector:
	# This example of a detector detects faces. However, you have annotations for ears!

	left_cascade = cv2.CascadeClassifier("cascades/haarcascade_mcs_leftear.xml")
	right_cascade = cv2.CascadeClassifier("cascades/haarcascade_mcs_rightear.xml")

	def detect_left(self, img):
		det_list = self.left_cascade.detectMultiScale(img, 1.05, 2)
		return det_list

	def detect_right(self, img):
		det_list = self.right_cascade.detectMultiScale(img, 1.05, 2)
		return det_list


if __name__ == '__main__':
	in_path = "D:\\Documents\\Faks\\MAG1\\SB\\Seminar1\\files\\data\\ears\\test\\"
	out_path = "D:\\Documents\\Faks\\MAG1\\SB\\Seminar1\\files\\detectors\\cascade_detector"
	files = os.listdir(in_path)
	for file in files:
		fname = in_path + file
		img = cv2.imread(fname)
		detector = Detector()
		detected_loc_left = detector.detect_left(img)
		detected_loc_right = detector.detect_right(img)
		if len(detected_loc_left) != 0 and len(detected_loc_right) != 0:
			detected_loc = np.concatenate((detected_loc_right, detected_loc_left))
		elif len(detected_loc_right) == 0:
			detected_loc = detected_loc_left
		else:
			detected_loc = detected_loc_right

		with open(os.path.join(out_path, "lab_out", file[:-3] + "txt"), 'w') as fout:
			for x, y, w, h in detected_loc:
				cv2.rectangle(img, (x,y), (x+w, y+h), (128, 255, 0), 4)
				fout.write("0 {} {} {} {}\n".format(x, y, w, h))

		cv2.imwrite(out_path + "im_out\\" + file + '.detected.jpg', img)
