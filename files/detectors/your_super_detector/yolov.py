import torch
import cv2
from PIL import Image
import os
import pandas


model = torch.hub.load('ultralytics/yolov5', 'custom',
                       path='D:\\Documents\\Faks\\MAG1\\SB\\Seminar1\\files\\detectors\\your_super_detector\\best_preprocessed.pt')


path = 'D:\\Documents\\Faks\\MAG1\\SB\\Seminar1\\files\\data\\ears\\test_preprocessed'
filelist = os.listdir(path)
imgs = []
for f in filelist[:]:
    if f.endswith(".png"):
        imgs.append(path + '\\' + f)

# Inference
results = model(imgs, size=640)  # includes NMS

for img, f in zip(results.xywhn, filelist):
    with open(os.path.join('D:\\Documents\\Faks\\MAG1\\SB\\Seminar1\\files\\detectors\\your_super_detector\\runs\\detect\\labels', f[:-3]+'txt'), 'w') as fout:
        for box in img.numpy():
            fout.write("0 {} {} {} {}\n".format(str(box[0]), str(box[1]), str(box[2]), str(box[3])))

# Results
results.print()
results.save()  # or .show()

print(results.xyxy[0])  # img1 predictions (tensor)
results.pandas().xyxy[0]  # img1 predictions (pandas)