import os.path
from os import listdir
from PIL import Image
import numpy as np
from torchvision import transforms
from torchvision.utils import save_image
import matplotlib.pyplot as plt


def normalize_xyhw(xmax, ymax, box):
    x = (float(box[1]) + float(box[3]) / 2) / xmax
    y = (float(box[2]) + float(box[4]) / 2) / ymax
    w = float(box[3]) / xmax
    h = float(box[4]) / ymax
    return x, y, w, h


def normalize_bbs():
    for directory in ["D:\\Documents\\Faks\\MAG1\\SB\\Seminar1\\files\\detectors\\your_super_detector\\runs\\detect\\labels"]: #
        files = listdir(directory)
        for file in files:
            with open(os.path.join(directory, file), 'r') as fin:
                lines = fin.readlines()
            with open(os.path.join(directory, file), 'w') as fout:
                for line in lines:
                    xyhw = line.split(" ")
                    x, y, w, h = normalize_xyhw(480.0, 360.0, xyhw)
                    fout.write("0 {} {} {} {}\n".format(x, y, w, h))


def denormalize_xyhw(xmax, ymax, box):
    w = float(box[3]) * xmax
    h = float(box[4]) * ymax
    x = float(box[1]) * xmax - (w / 2)
    y = float(box[2]) * ymax - (h / 2)
    return int(x), int(y), int(w), int(h)


def denormalize_bbs():
    path = 'D:\\Documents\\Faks\\MAG1\\SB\\Seminar1\\files\\detectors\\your_super_detector\\runs\\detect\\labels'
    files = listdir(path)
    for file in files:
        with open(os.path.join(path, file), 'r') as fin:
            lines = fin.readlines()
        with open(os.path.join(path, file), 'w') as fout:
            for line in lines:
                xyhw = line.split(" ")
                x, y, w, h = denormalize_xyhw(480.0, 360.0, xyhw)
                fout.write("0 {} {} {} {}\n".format(x, y, w, h))


def mean_pixel():
    in_path = 'D:\\Documents\\Faks\\MAG1\\SB\\Seminar1\\files\\data\\ears\\test'
    outpath = 'D:\\Documents\\Faks\\MAG1\\SB\\Seminar1\\files\\data\\ears\\test_preprocessed'
    images = listdir(in_path)
    means = []
    stds = []
    for image in images:
        img = Image.open(in_path + "\\" + image)
        img_np = np.array(img)

        transform = transforms.Compose([
            transforms.ToTensor()
        ])
        img_tr = transform(img)
        img_np = np.array(img_tr)
        mean, std = img_tr.mean([1, 2]), img_tr.std([1, 2])
        if len(mean.numpy()) == 3:
            means.append(mean.numpy())
        if len(std.numpy()) == 3:
            stds.append(std.numpy())

    m = np.average(means, axis=0)
    s = np.average(stds, axis=0)

    transform_norm = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(m, s)
    ])

    for image in images:
        img = Image.open(in_path + "\\" + image)
        img_tr = transform(img)
        if len(img_tr.numpy()) == 3:
            img_normalized = transform_norm(img)
            img_normalized = img_normalized.permute(1, 2, 0)
            img_np = np.array(img_normalized)
            im = Image.fromarray((img_np * 255).astype(np.uint8))

            im.save(outpath + "\\" + image)
        else:
            img.save(outpath + "\\" + image)


if __name__ == '__main__':
    denormalize_bbs()
    # mean_pixel()
